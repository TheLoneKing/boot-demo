package com.theloneking.bootdemo;

import static org.junit.Assert.assertEquals;

import com.theloneking.bootdemo.models.User;
import com.theloneking.bootdemo.services.UserService;

import org.junit.jupiter.api.Test;
import org.junit.platform.commons.annotation.Testable;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient(timeout = "70000") // Sets timeout to 70 seconds
@Testable
class RestEndpointsTest {

    @Autowired
    private WebTestClient webTestClient;

    @SpyBean
    private UserService userService;

    @Test
    void testGetUserById() {
        User expected = new User("123", "John Smith", 40, "Oberstgruppenfuhrer");
        webTestClient.get()
                .uri("/api/user/123")
                .exchange()
                .expectStatus().isOk()
                .expectBody(User.class)
                .consumeWith(response -> assertEquals(expected, response.getResponseBody()));
    }

    @Test
    void testGetAllUsers() {
        User user1 = new User("123", "John Smith", 40, "Oberstgruppenfuhrer");
        User user2 = new User("124", "Joe Blake", 28, "Nazi Agent");
        User user3 = new User("125", "Juliana Crain", 27, "Resistance Operative");
        User user4 = new User("126", "Nobusuke Tagomi", 65, "Trade Minister");

        webTestClient
                .get()
                .uri("/api/user/all")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(User.class)
                .contains(user1, user2, user3, user4)
                .hasSize(4);
    }

}
