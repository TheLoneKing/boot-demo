package com.theloneking.bootdemo.models;

import java.util.Objects;

public class User {
    private String id;
    private String name;
    private int age;
    private String role;

    public User(String id, String name, int age, String role) {
        super();
        this.id = id;
        this.name = name;
        this.age = age;
        this.role = role;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof User) {
            User that = (User) obj;
            return Objects.equals(this.getId(), that.getId())
                    && Objects.equals(this.getName(), that.getName())
                    && Objects.equals(this.getAge(), that.getAge())
                    && Objects.equals(this.getRole(), that.getRole());
        }
        return false;
    }
}
