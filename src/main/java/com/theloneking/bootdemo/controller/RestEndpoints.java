package com.theloneking.bootdemo.controller;

import com.theloneking.bootdemo.models.User;
import com.theloneking.bootdemo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api")
public class RestEndpoints {
    @Autowired
    private UserService userService;

    @GetMapping(value = "/user/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public Mono<User> getUser(@PathVariable String id) {
        return userService.getUserById(id);
    }

    @GetMapping(value = "/user/all", produces = {MediaType.APPLICATION_JSON_VALUE})
    public Flux<User> getAllUsers() {
        return userService.getAllUsers();
    }
}
