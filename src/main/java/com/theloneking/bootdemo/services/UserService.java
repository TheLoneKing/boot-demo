package com.theloneking.bootdemo.services;

import com.theloneking.bootdemo.models.User;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class UserService {
    public Mono<User> getUserById(String userId) {
        try {
            System.out.println("Waiting for 15 seconds...");
            Thread.sleep(15000);
            return Mono.just(new User(userId, "John Smith", 40, "Oberstgruppenfuhrer"));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Flux<User> getAllUsers() {
        try {
            System.out.println("Waiting for 15 seconds...");
            Thread.sleep(15000);

            User user1 = new User("123", "John Smith", 40, "Oberstgruppenfuhrer");
            User user2 = new User("124", "Joe Blake", 28, "Nazi Agent");
            User user3 = new User("125", "Juliana Crain", 27, "Resistance Operative");
            User user4 = new User("126", "Nobusuke Tagomi", 65, "Trade Minister");

            return Flux.just(user1, user2, user3, user4);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
